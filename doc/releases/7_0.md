---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#designated-technical-writers
---

# GitLab Cloud Native Chart 7.0

Along with the `16.0` release of GitLab, we have bumped the chart version to `7.0`.

## Summary of major changes

### Bundled certmanager

The bundled certmanager chart is upgraded from 1.5.4 to 1.11.1. Depending on your cluster and tooling this
may require manual interaction before upgrading.

Make sure your cluster version is supported by certmanager 1.11. The release supports Kubernetes 1.21 to
1.26 and OpenShift 4.8 to 4.13. See [certmanager supported releases](https://cert-manager.io/docs/installation/supported-releases/)
for more information.

The default certmanager configuration now uses the `acme.cert-manager.io/http01-edit-in-place` annotation.
As a result, certmanager will use the existing Ingresses to complete ACME challenges instead of creating
new ones. This change was made to ensure compatibility with Ingress controllers that need the `ingressClassName`
to be set.

OpenShift users may have to modify the Security Context Constraints to deploy certmanager 1.10+.
See [certmanager 1.10 release notes](https://cert-manager.io/docs/release-notes/release-notes-1.10/#on-openshift-the-cert-manager-pods-may-fail-until-you-modify-security-context-constraints)
for more information.

In case you deploy any certmanager custom resources not managed by the GitLab chart, or use additional
scripts or tooling related to cert-manager, please read through the potentially breaking changes of
[certmanager 1.6 to 1.11](https://cert-manager.io/docs/release-notes/) before upgrading.

## Upgrade path from 6.x

In order to upgrade to the `7.0` version of the chart, you first need to upgrade to the latest `6.11.x`
release of the chart. Check the [version mapping details](../installation/version_mappings.md) for the latest patch.

GitLab now defaults to using two database connections. Prior to upgrading, you can check that PostgreSQL `max_connections` is
high enough (using more than 50% of the available max connections).
You can verify this by running the following Rake task using [the Toolbox container](../charts/gitlab/toolbox/index.md#toolbox-included-tools):

```shell
gitlab-rake gitlab:db:decomposition:connection_status
```

If the task indicates that `max_connections` is high enough, then you can
proceed with the upgrade. If not, or you wish to remain on single
connection, you can set the `ci.enabled` key to `false` prior to the upgrade.
